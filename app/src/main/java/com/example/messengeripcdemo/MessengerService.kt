package com.example.messengeripcdemo

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.*
import android.widget.Toast

/** Command to the service to display a message  */
const val MSG_SAY_HELLO = 1

class MessengerService : Service() {


    lateinit var mMessenger: Messenger

    override fun onBind(intent: Intent?): IBinder? {
        Toast.makeText(applicationContext, "binding", Toast.LENGTH_SHORT).show()
        mMessenger = Messenger(IncomingHandler(this))
        return mMessenger.binder
    }


    class IncomingHandler : Handler {
        private lateinit var context: Context

        constructor(context: Context) : super() {
            this.context = context
        }

        override fun handleMessage(msg: Message) {
            val bundle = Bundle()
            when (msg.what) {
                MSG_SAY_HELLO -> {

                    val servicemsg: Message = Message.obtain(null, MSG_SAY_HELLO, 0, 0)
                    bundle.putString("msg", "hello ak it's service")
                    servicemsg.data = bundle

                    msg.replyTo.send(servicemsg)
//                    Toast.makeText(context, "hello ak from service", Toast.LENGTH_SHORT).show()

                }
                else -> super.handleMessage(msg)
            }
        }

    }
}